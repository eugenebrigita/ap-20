package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
    public Longbow() {
        this.weaponName = "Longbow";
        this.weaponValue = 15;
        this.weaponDescription = "Big Longbow";
    }

//    public String getName(){
//        return "Big Longbow";
//    }
//
//    public int getWeaponValue(){
//        return 15;
//    }
//
//    public String getDescription(){
//        return weaponDescription;
//    }
}
