package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
    public Sword() {
        this.weaponName = "Sword";
        this.weaponValue = 25;
        this.weaponDescription = "Great Sword";
    }

//    public String getName(){
//        return "Great Sword";
//    }
//
//    public int getWeaponValue(){
//        return 25;
//    }
//
//    public String getDescription(){
//        return weaponDescription;
//    }

}
