package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    private String name;
    private String role;

    public OrdinaryMember(String childName, String childRole) {
        this.name = childName;
        this.role = childRole;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        // ordinary member tidak boleh memiliki child member
    }

    @Override
    public void removeChildMember(Member member) {
        // ordinary member tidak boleh memiliki child member
    }

    @Override
    public List<Member> getChildMembers() {
        return new ArrayList<>();
    }
}
