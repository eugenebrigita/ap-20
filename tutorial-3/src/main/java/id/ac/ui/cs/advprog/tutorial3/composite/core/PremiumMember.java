package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    private String name;
    private String role;
    private List<Member> memberList = new ArrayList<>();

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if (role.equals("Master") || memberList.size() < 3) {
            memberList.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        if (memberList.size() != 0) {
            memberList.remove(member);
        }
    }

    @Override
    public List<Member> getChildMembers() {
        return memberList;
    }
}
