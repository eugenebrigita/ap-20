package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.List;

public class ChainSpell implements Spell {

    // Gabungan dari beberapa spell yang akan dijalankan berurutan
    private List<Spell> spells;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
    }

    // TODO: Complete Me
    @Override
    public void cast() {
        for (Spell spell : spells) spell.cast();
    }

    @Override
    public void undo() {
        // undo pada setiap spell yang menyusunnya dengan urutan terbalik
        for (int i = spells.size() - 1; i >= 0; i--) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
