package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    Spell latestSpell;
    int numOfUndo = 0;

    public void castSpell(String spellName) {
        // TODO: Complete Me
        if (!spells.containsKey(spellName)) {
            return;
        }
        Spell spell = spells.get(spellName);
        spell.cast();
        latestSpell = spell;
        numOfUndo = 0;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if (latestSpell == null) {
            // TODO: Tidak boleh
            System.out.println("Belum ada spell yang di cast!");
        } else {
            ++numOfUndo;
            if (numOfUndo >= 2) {
                System.out.println("Tidak boleh undo lebih dari satu kali.");
            } else {
                latestSpell.undo();
            }
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
