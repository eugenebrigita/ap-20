package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public abstract class HighSpiritSpell implements Spell {
	protected HighSpirit spirit;

    public HighSpiritSpell(HighSpirit spirit) {
        // TODO: Complete Me
        this.spirit = spirit;
    }

    @Override
    public void undo() {
        // TODO: Complete me
        // if (spirit.getPrevState() == HighSpiritState.ATTACK) spirit.attackStance();
        switch (spirit.getPrevState()) {
            case ATTACK:
                spirit.attackStance();
                break;
            case DEFEND:
                spirit.defenseStance();
                break;
            case STEALTH:
                spirit.stealthStance();
                break;
            case SEALED:
                spirit.seal();
                break;
            default:
                // impossible if prev state is valid
                break;
        }
    }

    @Override
    public String spellName() {
        return "High Spirit Spell";
    }
}
