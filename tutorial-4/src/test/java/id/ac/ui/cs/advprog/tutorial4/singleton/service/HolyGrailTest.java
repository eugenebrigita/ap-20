package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    HolyGrail holyGrail;

    @BeforeEach
    public  void setUp() throws Exception {
        holyGrail = new HolyGrail();
        holyGrail.makeAWish("I wanna go to cinema");
    }

    @Test
    public void testSetWish() {
        holyGrail.makeAWish("I wanna go to coffee shop");
        assertEquals("I wanna go to coffee shop", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void testGetWish() {
        HolyWish holyWish = holyGrail.getHolyWish();
        String wish = holyWish.getWish();
        assertEquals("I wanna go to cinema", wish);
    }
}
