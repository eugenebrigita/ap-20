package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        // TODO create test
        HolyWish holyWish2 = holyWish.getInstance();
        assertEquals(holyWish, holyWish2);
    }

    @Test
    public void testWish(){
        // TODO create test
        holyWish.setWish("I want to have a great vacation");
        assertEquals("I want to have a great vacation", holyWish.getWish());
    }

    @Test
    public void testWishExist(){
        // TODO create test
        HolyWish holyWish2 = holyWish.getInstance();

        holyWish2.setWish("I want to graduate hehe");
        assertNotNull(holyWish2.getWish());
    }

}
